#!/usr/bin/python3

import urllib.request
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys

class CamHandler(ContentHandler):
    def __init__(self):
        self.inCamara = False
        self.inContent = False
        self.content = ""
        self.lugar = ""
        self.id = ""
        self.src = ""
        self.coordenadas = ""
        self.cams = []
    def startElement(self, name, attrs):
        if name == 'camara':
            self.inCamara = True
        elif self.inCamara:
            if name == 'lugar':
                self.inContent = True
            elif name == 'coordenadas':
                self.inContent = True
            elif name == 'src':
                self.inContent = True
            elif name == 'id':
                self.inContent = True


    def endElement(self, name):
        global cams
        if name == 'camara':
            self.inCamara = False
            self.cams.append({'id':self.id,
                              'src':self.src,
                              'lugar':self.lugar,
                              'coordenadas':self.coordenadas,
                              })
        elif self.inCamara:
            if name == 'lugar':
                self.lugar = self.content
                self.content = ""
                self.inContent = False
            elif name == 'id':
                self.id = self.content
                self.content = ""
                self.inContent = False
            elif name == 'coordenadas':
                self.coordenadas = self.content
                self.content = ""
                self.inContent = False
            elif name == 'src':
                self.src = self.content
                self.content = ""
                self.inContent = False



    def characters(self, chars):
        if self.inContent:
            self.content = self.content + chars


class CamCanal1:
    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = CamHandler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)

    def cams(self):
        return self.handler.cams

if __name__ == '__main__':
    stream = urllib.request.urlopen("https://gitlab.eif.urjc.es/cursosweb/2023-2024/final-teveo/-/raw/main/listado1.xml")
    cams = CamCanal1(stream.url).cams()
    for cam in cams:
        print(cam['id'])