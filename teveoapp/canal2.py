#!/usr/bin/python3

import urllib.request
from xml.sax.handler import ContentHandler
from xml.sax import make_parser


class CamHandler(ContentHandler):
    def __init__(self):
        self.inList = False
        self.inCamara = False
        self.inContent = False
        self.inPlace = False
        self.content = ""
        self.longitude = ""
        self.latitude = ""
        self.place = ""
        self.url = ""
        self.id = ""
        self.info = ""
        self.cams = []

    def startElement(self, name, attrs):
        if name == 'list':
            self.inList = True
        elif self.inList:
            if name == 'cam':
                self.inCamara = True
                self.id = attrs.get('id')
            elif self.inCamara:
                if name == 'url':
                    self.inContent = True
                elif name == 'info':
                    self.inContent = True
                elif name == 'place':
                    self.inPlace = True
                elif self.inPlace:
                    if name == 'latitude':
                        self.inContent = True
                    elif name == 'longitude':
                        self.inContent = True

    def endElement(self, name):
        global cams

        if name == 'list':
            self.inList = False
        elif self.inList:
            if name == 'cam':
                self.inCamara = False
                self.cams.append({'id': self.id,
                                  'src': self.url,
                                  'coordenadas': self.latitude + ',' + self.longitude,
                                  'lugar': self.info,
                                  })
            elif self.inCamara:
                if name == 'info':
                    self.info = self.content
                    self.content = ""
                    self.inContent = False
                elif name == 'url':
                    self.url = self.content
                    self.content = ""
                    self.inContent = False
                elif name == 'place':
                    self.inPlace = False
                elif self.inPlace:
                    if name == 'latitude':
                        self.latitude = self.content
                        self.content = ""
                        self.inContent = False
                    elif name == 'longitude':
                        self.longitude = self.content
                        self.content = ""
                    self.inContent = False

    def characters(self, chars):
        if self.inContent:
            self.content += chars


class CamCanal2:
    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = CamHandler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)

    def cams(self):
        return self.handler.cams

if __name__ == '__main__':
    stream = urllib.request.urlopen("https://gitlab.eif.urjc.es/cursosweb/2023-2024/final-teveo/-/raw/main/listado2.xml")
    cams = CamCanal2(stream.url).cams()
    for cam in cams:
        print(cam['id'])

