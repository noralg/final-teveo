# Generated by Django 5.0.3 on 2024-06-19 06:38

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("teveoapp", "0004_alter_comentador_id"),
    ]

    operations = [
        migrations.RenameField(
            model_name="camara",
            old_name="sum_comentarios",
            new_name="num_comentarios",
        ),
        migrations.RenameField(
            model_name="comentador",
            old_name="font_family",
            new_name="letra_family",
        ),
        migrations.RenameField(
            model_name="comentador",
            old_name="font_size",
            new_name="letra_size",
        ),
        migrations.RemoveField(
            model_name="comentador",
            name="name",
        ),
        migrations.AddField(
            model_name="comentador",
            name="nombre",
            field=models.CharField(default="Anónimo", max_length=100),
        ),
        migrations.AlterField(
            model_name="comentador",
            name="id",
            field=models.CharField(max_length=256, primary_key=True, serialize=False),
        ),
        migrations.CreateModel(
            name="Voto",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "megusta",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="teveoapp.camara",
                    ),
                ),
            ],
        ),
        migrations.DeleteModel(
            name="Vote",
        ),
    ]
