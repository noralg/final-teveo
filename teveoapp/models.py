from django.db import models
from django.utils.translation import gettext_lazy as _

# Create your models here.
class Camara(models.Model):
    id = models.CharField(max_length=100, primary_key=True)
    src = models.TextField()
    lugar = models.CharField(max_length=300)
    coordenadas = models.CharField(max_length=200)

class Comentario(models.Model):
    camara = models.ForeignKey(Camara, on_delete=models.CASCADE)
    comentador = models.CharField(max_length=100)
    comentario = models.TextField()
    fecha = models.DateTimeField(auto_now_add=True)


class Configuracion(models.Model):
    nombre_usuario = models.CharField(max_length=50, default='Anónimo')
    TAMANO_FUENTE_CHOICES = [
        (10, _('Small')),
        (12, _('Medium')),
        (16, _('Large')),
        (26, _('X-Large'))
    ]
    tamano_fuente = models.IntegerField(choices=TAMANO_FUENTE_CHOICES, default=None)
    TIPO_FUENTE_CHOICES = [
        ('Arial', 'Arial'),
        ('Helvetica', 'Helvetica'),
        ('Monospace', 'Monospace'),
        ('Verdana', 'Verdana'),
        ('Times new roman', 'Times New Roman'),
    ]
    tipo_fuente = models.CharField(max_length=50, choices=TIPO_FUENTE_CHOICES, default=None)


class Like(models.Model):
    megusta = models.ForeignKey(Camara, on_delete=models.CASCADE, default=None)