from django.utils.translation import gettext_lazy as _
from django import forms
from .models import Configuracion

class ConfiguracionForm(forms.Form):
    nombre_usuario = forms.CharField(label=_('Nombre'), max_length=50)
    tamano_fuente = forms.ChoiceField(label=_('Tamaño de Fuente'), choices=Configuracion.TAMANO_FUENTE_CHOICES)
    tipo_fuente = forms.ChoiceField(label=_('Tipo de Fuente'), choices=Configuracion.TIPO_FUENTE_CHOICES)
