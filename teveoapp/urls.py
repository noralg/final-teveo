from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('camaras', views.camaras, name='camaras'),
    path('comentario', views.crear_comentario, name='comentario'),
    path('configuracion', views.manejar_configuracion, name='configuracion'),
    path('autorizar', views.autorizar, name='autorizar_navegador'),
    path('help', views.ayuda, name='help'),
    path('logout', views.logout_view, name='logout'),
    path('json/<str:id>', views.camara_json, name='camara_json'),
    path('<str:id_camara>dyn', views.camara_dinamica, name='camara_dinamica'),
    path('<str:id>', views.camara, name='camara'),
] + static(settings.STATIC_URL)

