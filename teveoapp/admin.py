from django.contrib import admin
from .models import Camara, Comentario, Configuracion, Like

# Register your models here.
admin.site.register(Camara)
admin.site.register(Comentario)
admin.site.register(Configuracion)
admin.site.register(Like)



