from django.test import TestCase, Client
from django.urls import reverse
from django.utils import timezone
from .models import Camara, Comentario, Configuracion, Like
from .forms import ConfiguracionForm

# Create your tests here.
class GetTests(TestCase):
    def test_camara(self):
        camara = Camara.objects.create(id=1, src='http://example.com', lugar='Madrid', coordenadas='123,345', num_comentarios=0)
        self.assertEqual(camara.num_comentarios, 0)

    def test_index(self):
        url = reverse('index')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')
    def verificar_comentarios(self, url_name):
        comentarios = [
            Comentario.objects.create(camara=self.camara, comentador='Víctor', comentario='Comentario de prueba nº1', fecha=timezone.now()),
            Comentario.objects.create(camara=self.camara, comentador='Óscar', comentario='Comentario de prueba nº2', fecha=timezone.now())
        ]

        url = reverse(url_name, args=[self.camara.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        for comentario in comentarios:
            self.assertIn(comentario.comentario.encode(), response.content)

    def test_camaras_view_post(self):
        client = Client()
        self.camara = Camara.objects.create(id='2', src='http://example.com', lugar='Madrid', coordenadas='123,345', num_comentarios=0)
        url = reverse('camaras') #simulamos dar un megusta
        data = {'id': self.camara.id, 'src': self.camara.src, 'lugar': self.camara.lugar, 'coordenadas':self.camara.coordenadas, 'num_comentarios':self.camara.num_comentarios }
        response = client.post(url, data)
        self.assertEqual(response.status_code, 200)

        likes_count = Like.objects.filter(megusta=self.camara).count()
        self.assertEqual(likes_count, 1)
    def test_camaras(self):
        url = reverse('camaras')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'camaras.html')

    def test_camara_json_view(self):
        client = Client()
        self.camara = Camara.objects.create(id='1', src='http://example.com', lugar='Madrid', coordenadas='123,345', num_comentarios=0)
        url = reverse('camara_json', args=[self.camara.id])
        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'], 'application/json')

        expected_data = {'id': self.camara.id, 'src': self.camara.src, 'lugar': self.camara.lugar, 'coordenadas': self.camara.coordenadas, 'num_comentarios': 0}
        self.assertDictEqual(expected_data, response.json())

class ConfiguracionTestCase(TestCase):

    def test_configuracion_form(self):
        client = Client()
        form_data = {'nombre_usuario': 'Juan', 'tamano_fuente': 12, 'tipo_fuente': 'Arial'}

        form = ConfiguracionForm(data=form_data)
        self.assertTrue(form.is_valid())

        response = client.post(reverse('configuracion'), data=form_data)
        self.assertRedirects(response, reverse('index'))

        configuracion_guardada = Configuracion.objects.latest('id')
        self.assertEqual(configuracion_guardada.nombre_usuario, 'Juan')
        self.assertEqual(configuracion_guardada.tamano_fuente, 12)
        self.assertEqual(configuracion_guardada.tipo_fuente, 'Arial')
