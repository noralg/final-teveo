import base64
import random
import ssl
import urllib.request
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from django.db.models import Count
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import logout
from django.core.paginator import Paginator
from .forms import ConfiguracionForm
from .models import Comentario, Camara, Configuracion, Like
from .canal1 import CamCanal1
from .canal2 import CamCanal2

def autorizar(request):
    if 'session_key' in request.GET:
        session_key = request.GET['session_key']
        request.session['authorized_session_key'] = session_key
        return  HttpResponse("Navegador autorizado correctamente ")
    return HttpResponse("Error: No se proporcionó ID para la sesión actual.")

def index(request):

    config_actual, created = Configuracion.objects.get_or_create(defaults={'nombre_usuario':'Anónimo','tamano_fuente': 12, 'tipo_fuente':'Arial'})
    nombre_usuario = config_actual.nombre_usuario or 'Anónimo'

    list_comentarios = Comentario.objects.all().order_by('-fecha')
    total_comentarios = Comentario.objects.count
    total_camaras = Camara.objects.count()

    paginator = Paginator(list_comentarios, 5)
    num_pags = request.GET.get('page')
    pag_comentarios = paginator.get_page(num_pags)


    context = {
        'nombre_usuario': nombre_usuario,
        'total_camaras':total_camaras,
        'total_comentarios': total_comentarios,
        'pag_comentarios':pag_comentarios,
        'config_actual':config_actual,
    }

    response = render(request, 'index.html', context=context)
    return HttpResponse(response)

def camara_dinamica(request, id_camara):
    camara = get_object_or_404(Camara, id=id_camara)
    list_comentarios = Comentario.objects.filter(camara=camara)

    config_actual, created = Configuracion.objects.get_or_create(
        defaults={'nombre_usuario': 'Anónimo', 'tamano_fuente': 12, 'tipo_fuente': 'Arial'})
    nombre_usuario = config_actual.nombre_usuario or 'Anónimo'


    image = download_image(camara.src)
    if image is None:
        return JsonResponse({'error': 'No se pudo descargar la imagen'}, status=500)
    else:
        image_base64 = base64.b64encode(image).decode('utf-8')

    total_camaras = Camara.objects.all().count()
    total_comentarios = Comentario.objects.all().count()

    list_comentarios = list_comentarios.order_by('-fecha')


    # contexto para la renderizacion de la plantilla
    context = {
        'id': id_camara,
        'image': image,
        'image_base64': image_base64,
        'nombre_usuario': nombre_usuario,
        'config_actual':config_actual,
        'comentarios':list_comentarios,
        'total_camaras': total_camaras,
        'total_comentarios': total_comentarios,

    }
    response = render(request, 'camara-dyn.html', context=context)
    return HttpResponse(response)


def crear_comentario(request):
    id = request.GET.get('id')
    camara = get_object_or_404(Camara, id=id)


    config_actual, created = Configuracion.objects.get_or_create(
        defaults={'nombre_usuario': 'Anónimo', 'tamano_fuente': 12, 'tipo_fuente': 'Arial'})
    nombre_usuario = config_actual.nombre_usuario or 'Anónimo'


    if nombre_usuario == "":
        nombre_usuario = "Anónimo"

    if request.method == 'POST':
        texto_comentario = request.POST['comentario_texto']
        comentario = camara.comentario_set.create(comentador=nombre_usuario, fecha=timezone.now(), comentario=texto_comentario)
        comentario.save()

    total_camaras = Camara.objects.all().count()
    total_comentarios = Comentario.objects.all().count

    context = {
        'imagen':camara.src, # no se lo paso a comentario
        'lugar': camara.lugar,
        'coordenadas': camara.coordenadas,
        'nombre_usuario': nombre_usuario,
        'config_actual': config_actual,
        'total_camaras': total_camaras,
        'total_comentarios': total_comentarios,
    }
    response = render(request, 'comentario.html', context=context)
    return HttpResponse(response)

def read_canales(url_xml):
    url = url_xml
    if url == "https://gitlab.eif.urjc.es/cursosweb/2023-2024/final-teveo/-/raw/main/listado1.xml":
        context = ssl._create_unverified_context()
        stream_xml = urllib.request.urlopen(url, context=context)
        canal = CamCanal1(stream_xml)
    elif url == "https://gitlab.eif.urjc.es/cursosweb/2023-2024/final-teveo/-/raw/main/listado2.xml":
        context = ssl._create_unverified_context()
        stream_xml = urllib.request.urlopen(url, context=context)
        canal = CamCanal2(stream_xml)

    ids_camaras_existentes = Camara.objects.values_list('id', flat=True)
    for cam in canal.cams():
        cam = Camara(id=cam['id'], src=cam['src'], lugar=cam['lugar'],  coordenadas=cam['coordenadas'])
        if cam.id not in ids_camaras_existentes:
            print(f"Se ha descargado una nueva cámara con ID {cam.id}")
            cam.save()
        else:
            print(f"Ya se han descargado estos datos.")

def camara(request, id):
    camara = get_object_or_404(Camara, id=id)

    num_camaras = Camara.objects.all()
    total_camaras = Camara.objects.all().count()
    total_comentarios = Comentario.objects.all().count

    config_actual, created = Configuracion.objects.get_or_create(
        defaults={'nombre_usuario': 'Anónimo', 'tamano_fuente': 12, 'tipo_fuente': 'Arial'})
    nombre_usuario = config_actual.nombre_usuario or 'Anónimo'

    list_comentarios = Comentario.objects.filter(camara=camara).order_by('-fecha')
    paginator = Paginator(list_comentarios, 5)
    num_pags = request.GET.get('page')
    pag_comentarios = paginator.get_page(num_pags)

    context = {
        'id': camara.id,
        'imagen': camara.src,
        'lugar': camara.lugar,
        'coordenadas': camara.coordenadas,
        'comentarios': list_comentarios,
        'nombre_usuario': nombre_usuario,
        'config_actual': config_actual,
        'pag_comentarios': pag_comentarios,
        'num_camaras': num_camaras,
        'total_camaras': total_camaras,
        'total_comentarios': total_comentarios,

    }
    response = render(request, 'camara.html', context=context)
    return HttpResponse(response)

def camaras(request):
    fuente_datos = [
        "https://gitlab.eif.urjc.es/cursosweb/2023-2024/final-teveo/-/raw/main/listado1.xml",
        "https://gitlab.eif.urjc.es/cursosweb/2023-2024/final-teveo/-/raw/main/listado2.xml",
    ]

    config_actual, created = Configuracion.objects.get_or_create(
        defaults={'nombre_usuario': 'Anónimo', 'tamano_fuente': 12, 'tipo_fuente': 'Arial'})
    nombre_usuario = config_actual.nombre_usuario or 'Anónimo'

    if request.method == 'POST':
        if 'list_url' in request.POST:
            url = request.POST.get('list_url')
            read_canales(url)
        elif 'id' in request.POST:
            camara_id = request.POST.get('id')
            camara = get_object_or_404(Camara, id=camara_id)
            like = Like(megusta=camara)
            like.save()

    num_camaras = Camara.objects.all()
    total_camaras = Camara.objects.all().count()
    total_comentarios = Comentario.objects.all().count

    camaras_ordenadas = Camara.objects.annotate(num_comment=Count('comentario')).order_by('-num_comment')
    paginator = Paginator(camaras_ordenadas, 5)
    num_pags = request.GET.get('page')
    pag_camaras = paginator.get_page(num_pags)

    if len(num_camaras) == 0:
        context = {
            'datos': fuente_datos,
            'nombre_usuario': nombre_usuario,
            'config_actual': config_actual,
            'total_camaras': total_camaras,
            'total_comentarios': total_comentarios,
        }
        return render(request, 'camaras.html', context=context)

    else:
        imagen_cam = random.choice(Camara.objects.values_list('src', flat=True))

        for camara in num_camaras:
            camara.num_comentarios = Comentario.objects.filter(camara=camara).count()
            camara.megusta = Like.objects.filter(megusta=camara).count()

        context = {
            'imagen_cam': imagen_cam,
            'datos': fuente_datos,
            'nombre_usuario': nombre_usuario,
            'config_actual':config_actual,
            'num_camaras': num_camaras,
            'total_camaras': total_camaras,
            'total_comentarios': total_comentarios,
            'pag_camaras': pag_camaras,
        }

    response = render(request, 'camaras.html', context=context)
    return HttpResponse(response)

def camara_json(request, id):
    camara = Camara.objects.get(id=id)
    num_comentarios = Comentario.objects.filter(camara=camara).count()

    context = {
        'id': camara.id,
        'src': camara.src,
        'lugar': camara.lugar,
        'coordenadas': camara.coordenadas,
        'num_comentarios': num_comentarios,
    }

    return JsonResponse(context, safe=False)

headers = {"User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/118.0"}

def download_image(url_imagen):
    request = urllib.request.Request(url=url_imagen, headers=headers)
    try:
        with urllib.request.urlopen(request) as response:
            image = response.read()
    except urllib.error.URLError:
        return None
    return image

def manejar_configuracion(request):
    session_key = request.session.session_key
    config_actual, created = Configuracion.objects.get_or_create(
        defaults={'nombre_usuario': 'Anónimo', 'tamano_fuente': 12, 'tipo_fuente': 'Arial'})
    nombre_usuario = config_actual.nombre_usuario or 'Anónimo'

    if config_actual is None:
        config_actual = Configuracion()

    if request.method == 'POST':
        form = ConfiguracionForm(request.POST)
        if form.is_valid():
            config_actual.nombre_usuario = form.cleaned_data['nombre_usuario']
            config_actual.tamano_fuente = int(form.cleaned_data['tamano_fuente'])
            config_actual.tipo_fuente = form.cleaned_data['tipo_fuente']
            config_actual.save()
            return HttpResponseRedirect("/")
    else:
        form = ConfiguracionForm(initial={'nombre_usuario':config_actual.nombre_usuario,
                                          'tamano_fuente': config_actual.tamano_fuente,
                                          'tipo_fuente': config_actual.tipo_fuente
                                          })

    total_camaras = Camara.objects.all().count()
    total_comentarios = Comentario.objects.all().count

    context = {
        'config_actual':config_actual,
        'form': form,
        'session_key': session_key,
        'total_camaras': total_camaras,
        'total_comentarios': total_comentarios,
    }

    response = render(request, 'configuracion.html', context=context)
    return HttpResponse(response)

def ayuda(request):
    config_actual, created = Configuracion.objects.get_or_create(
        defaults={'nombre_usuario': 'Anónimo', 'tamano_fuente': 12, 'tipo_fuente': 'Arial'})
    nombre_usuario = config_actual.nombre_usuario or 'Anónimo'


    total_camaras = Camara.objects.all().count()
    total_comentarios = Comentario.objects.all().count


    list_comentarios = Comentario.objects.all().order_by('-fecha')
    paginator = Paginator(list_comentarios, 5)
    num_pags = request.GET.get('page')
    pag_comentarios = paginator.get_page(num_pags)

    context = {
        'nombre_usuario': nombre_usuario,
        'config_actual': config_actual,
        'total_camaras': total_camaras,
        'total_comentarios': total_comentarios,
        'pag_comentarios': pag_comentarios,
    }
    response = render(request, 'ayuda.html', context=context)
    return HttpResponse(response)

def logout_view(request):
    logout(request)
    Configuracion.objects.all().delete()
    request.session.flush()
    return HttpResponseRedirect('/')



