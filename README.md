# Final-TeVeO

Repositorio de plantilla para el proyecto final del curso 2023-2024.



# ENTREGA CONVOCATORIA JUNIO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Nora Larrañaga García
* Titulación: Ingeniería en Sistemas de Telecomunicación
* Cuenta en laboratorios: noralg
* Cuenta URJC: n.larranaga.2021@alumnos.urjc.es
* Video básico (url): https://youtu.be/1AY2feunizk
* Video parte opcional (url): https://youtu.be/zGt2gKrnnSc
* Despliegue (url): http://noralg.pythonanywhere.com/
* Contraseñas: 
* Cuenta Admin Site: noralg/admin

## Resumen parte obligatoria
* Página principal: Página es la que es posible visualizar un listado de los comentarios puestos de más nuevos a más antiguos y paginados.
Cada comentario va asociado con la imagen a la cual se comentó, el ID de la cámara a la que pertenece dicha imagen, (enlace redirigido a esta), y la fecha de publicación del susodicho comentario.
* Página de cámaras: Página que su inicialización desde 0 presenta dos enlaces; dos fuentes de datos: 'Listado 1' y 'Listado 2'.
En función del listado descargado se mostrarán las cámaras asociadas con siete atributos: - ID cámara - Lugar - Coordenadas - Enlace a poner a comentar - Número de comentarios - Cámara dinámica - Número de 'Me gustas'
Al principio de esta página se muestra, si se han descargado las imágenes en la base de datos, una imagen aleatoria de entre todas ellas.
El orden del listado se rige en base al número de comentarios que posea la cámara (más comentarios más arriba se situará en la lista), estando paginado en funcíon del número de comentarios.
* Página de cada cámara: toda información asociada a la cámara, comentarios puestos en ella y sus fechas ordenados de más reciente a más antiguo, enlace para escribir un comentario, acceder a su página dinámica y a su página en formato JSON.
* Página para poner un comentario: Página para escribir un comentario a una cámara concreta. En ella se permiten visualizar todos los comentarios puestos con anterioridad.
* Página dinámica: similar a la página de cada cámara pero con la incorporación de que en esta página la imagen se actualizará cada 30 segundos.
* Página de configuración: Página compuesta con un formulario para elegir el nombre de usuario, el tamaño de la letra y el tipo de letra.
El nombre de usuario está diseñado para que sea únicamente visible en el menú desplegable.
Mediante una clave de sesión es posible, con autorización previa, exportar la configuración a otros navegadores.
* Página de ayuda: breve explicación de cada recurso que conforman la webapp.
* Página de acceso al 'Admin Site'
## Lista partes opcionales

* Nombre parte: Inclusión de un favicon del sitio
* Nombre parte: Permitir que las sesiones se puedan terminar; se borre la configuración si esta se cambió.
* Nombre parte: paginado en función de comentario y listas de cámaras.
* Nombre parte: Posibilidad de dar like a cualquier cámara.
